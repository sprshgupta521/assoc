import { Body, Controller, Get, Post,Param,Delete } from '@nestjs/common';
import { UserService,FeedService,TranService } from './cats.service';
import { UserDto } from './dto/create.user.dto';
import { tranDto} from './dto/create.transaction.dto';
import {feedDto} from './dto/create.feed.dto';
import { User } from './entites/user.entities';
import{ Trans } from './entites/transactions.entities';
import {Feed } from './entites/feed.entities';

@Controller('user')
export class UserController {
  constructor(private readonly UserService: UserService) {}

  @Post()
  async create(@Body() UserDto: UserDto) {
    return await this.UserService.create(UserDto);
  }

  @Get()
  async findAll() {
    return await this.UserService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<User> {
    return this.UserService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.UserService.remove(id);
  }
}

@Controller('tran')
export class TranController {
  constructor(private readonly TranService: TranService) {}

  @Post()
  async create(@Body() tranDto: tranDto) {
    return await this.TranService.create(tranDto);
  }

  @Get()
  async findAll() {
    return await this.TranService.findAll();
  }

}

@Controller('feed')
export class FeedController {
  constructor(private readonly FeedService: FeedService) {}

  @Post()
  async create(@Body() feedDto: feedDto) {
    return await this.FeedService.create(feedDto);
  }

  @Get()
  async findAll() {
    return await this.FeedService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Feed> {
    return this.FeedService.findOne(id);
  }
}

