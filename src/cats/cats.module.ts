import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { from } from 'rxjs';
import { FeedController, TranController, UserController } from './cats.controller';
import { User} from './entites/user.entities';
import { Feed } from './entites/feed.entities';
import { Trans } from './entites/transactions.entities';
import{ SequelizeModule} from '@nestjs/sequelize';
import { FeedService, TranService, UserService } from './cats.service';

@Module({
  imports: [SequelizeModule.forFeature([User,Trans,Feed])],
  controllers: [UserController,TranController,FeedController],
  providers: [UserService,TranService,FeedService],
})
export class CatsModule {}