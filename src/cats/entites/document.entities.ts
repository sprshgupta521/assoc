import { Column, DataType, HasOne, Model, Table } from "sequelize-typescript";
import { gurantee } from "./gurantee.entities";

 @Table
 export class document extends Model<document>{

    @Column({primaryKey: true,type: DataType.UUIDV4})
    id: string;

    @HasOne(()=>gurantee,'gurantee_id')
    gurantee_id: gurantee;
 }