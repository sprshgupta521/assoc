import { extend } from "joi";
import { model } from "mongoose";
import { compileFunction } from "node:vm";
import { Table,Model, Column, DataType, BelongsTo, HasOne } from "sequelize-typescript";
import { gurantee } from "./gurantee.entities";


@Table
export class tender extends Model<tender>{

    @Column({primaryKey: true,type: DataType.UUIDV4})
      id: string;

    @Column
    tenderDate: Date;
    
    @Column
    refpNumber: string;
    @Column
    tenderTitle: string;

    @Column
    descrpition: string;

    @Column
    tenderDocumentUrl: string;

    @Column
    awardLetterRefnumber: string;

    @Column
    awardLetterUrl: string;


    @Column({type: DataType.STRING(255)})
    tenderDocumentLink: string;

    @Column({type: DataType.STRING(45)})
    awardLetterDate:string;

    @Column
    validateRpfnumber:string;

    @Column
    validateTenderTitle:string;

    @Column
    validateDescription: string;

    @Column
    validateTenderDocumentUrl: string;

    @Column
    validateAwardLetterUrl: string;


    @HasOne(()=>gurantee,'gurantee_id')
    gurantee_id: gurantee;

}