
import { Table,Column,Model, HasOne, BelongsTo, DataType } from 'sequelize-typescript';
import { from } from 'rxjs';
import { gurantee } from './gurantee.entities';

@Table
export class Fees extends Model<Fees>{
  
  @Column({primaryKey: true,type: DataType.UUIDV4})
  id: string;

  @Column({type: DataType.FLOAT})
  commission: number;

  @Column({type: DataType.FLOAT})
  cashCover: number;

  @Column
  feescol: string;

  @Column({type:DataType.STRING(255)})
  acceptanceOfOfferDocumentUrl: string;

  @Column
  validateCommission: string;

  @Column
  validateCashCover: string;

  @Column
  validateacceptanceOfOfferDocumentUrl: string;


  @HasOne(()=> gurantee,'gurantee_id')
  gurantee_id: gurantee;
}
