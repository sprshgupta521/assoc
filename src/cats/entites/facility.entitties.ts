import { Column, DataType, Table,Model, BelongsTo, HasOne } from "sequelize-typescript";
import { gurantee } from "./gurantee.entities";


@Table
export class facility extends Model<facility>{
    @Column({primaryKey: true,type:DataType.UUIDV4})
    id: string;

    @Column({type:DataType.STRING(45)})
    amountRequired: string;

    @Column({type:DataType.STRING(45)})
    validityPeriod: string;

    @Column
    effectiveDate: Date;

    @Column
    validateAmountRequired: string;

    @Column
    validateType: string;

    @Column
    validateEffectiveDate: string;

    @Column
    validateValidatePeriod: string;

    @HasOne(()=>gurantee,'gurantee_id')
    gurantee_id: gurantee;
}