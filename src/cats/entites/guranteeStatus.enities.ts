import { isBtcAddress } from "class-validator";
import { Address } from "node:cluster";
import { from } from "rxjs";
import { BelongsTo, Column, DataType, Table,Model, HasOne, HasMany } from "sequelize-typescript";
import { SmallIntegerDataType, TinyIntegerDataType } from "sequelize/types";
import { values } from "sequelize/types/lib/operators";
import { gurantee} from './gurantee.entities';

@Table

export class guranteeStatus extends Model<guranteeStatus>
{
    @Column({primaryKey: true,type: DataType.UUIDV4})
    id: string;

    @Column({type:DataType.ENUM
        })
    status:string ;

    @Column( DataType.TEXT)
    first_name:string;

    @Column
    isActive:TinyIntegerDataType;

    @Column
    active: boolean;

    @HasMany(()=>gurantee,'gurantee_id')
     gurantee_id: gurantee;
        
}