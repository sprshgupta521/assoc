import { Column, Table, Model, HasOne, HasMany, DataType, BelongsTo } from "sequelize-typescript";
import { SMALLINT, SmallIntegerDataType } from "sequelize/types";
import {  guranteeStatus } from "./guranteeStatus.enities";
import { buyerDetails} from "./buyer.entities";
import { Fees } from "./fees.entities";
import { facility } from "./facility.entitties";
import { tender } from "./tender.entities";
import { document } from "./document.entities";


@Table({
    tableName: 'gurantee',
})
export class gurantee extends Model<gurantee>{
    // UUID v4
   @Column({primaryKey:true,type:DataType.UUIDV4})
   id: string;

   
    @Column({type:DataType.ENUM})
    type: string;
// one to many
    @HasMany(()=>guranteeStatus,'gurantee_id')
    gurantee_id:guranteeStatus;

    @BelongsTo(()=>document,'document_id')
    document_id: document;

    @BelongsTo(()=>Fees,'fees_id')
    fees_id: Fees;

    @BelongsTo(()=>buyerDetails,'buyerDetails_id')
    buyerDetails_id: buyerDetails;

    @BelongsTo(()=> facility,'facility_id')
    facility_id: facility;

    @BelongsTo(()=>tender,'tender_id')
    tender_id: tender;
}
