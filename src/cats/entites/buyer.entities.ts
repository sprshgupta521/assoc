
import { BelongsTo, Column,DataType,HasMany,HasOne,Model, Table } from "sequelize-typescript";
import { SmallIntegerDataType } from "sequelize/types";
import { gurantee } from "./gurantee.entities";


@Table
export class buyerDetails extends Model<buyerDetails>{

@Column({primaryKey:true,type:DataType.UUIDV4})
id: string;    

@Column({type: DataType.SMALLINT})
ValidatebuyerId: string;

@Column({primaryKey: false,type:DataType.STRING(45)})
buyerId?: string;

@Column
ValidBuyername: string;

@Column
ValidFirstName: string;

@Column
ValidLastName: string;

@Column
ValidAddress: string;

@Column
ValidPostCode: string;

@Column
Validcountry: string;

@Column
Validcounty: string;

@HasOne(()=> gurantee,'gurantee_id')
gurantee_id: gurantee;
}