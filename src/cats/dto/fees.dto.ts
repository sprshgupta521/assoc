export class feesDto{
    
  readonly commission: number;
  readonly cashCover: number;
  readonly feescol: string;
  readonly acceptanceOfOfferDocumentUrl: string;
  readonly validateCommission: string;
  readonly validateCashCover: string;
  readonly validateacceptanceOfOfferDocumentUrl: string;
}