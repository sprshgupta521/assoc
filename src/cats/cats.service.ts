import { Injectable,Inject } from '@nestjs/common';
import { tranDto} from './dto/create.transaction.dto';
import {feedDto} from './dto/create.feed.dto';
import { UserDto } from './dto/create.user.dto';
import { User } from './entites/user.entities';
import{ Trans } from './entites/transactions.entities';
import {Feed } from './entites/feed.entities';
import { from } from 'rxjs';
import { InjectModel } from '@nestjs/sequelize';
import { Transaction } from 'sequelize/types';
@Injectable()
export class UserService {
  constructor(
    @InjectModel(User) private readonly UserModel: typeof User, 
  ) {}

  async create(UserDto: UserDto): Promise<User> {
    const user = new User();
    user.firstname = UserDto.firstname;
    user.lastname = UserDto.lastname;
    user.username = UserDto.username;
    user.transaction = UserDto.transaction;
  

    return await user.save();
    
  }

  async findAll(): Promise<User[]> {
    return this.UserModel.findAll(
      {include:[{model:Trans}]}
    );
  }

  findOne(id: string): Promise<User> {
    return this.UserModel.findOne({
      where: {
        id,
      },
    });
  }

  async remove(id: string): Promise<void> {
    const user = await this.findOne(id);
    await user.destroy();
  }
}

@Injectable()
export class TranService {
  constructor(
    @InjectModel(Trans) private readonly TransModel:typeof  Trans, 
  ) {}

  async create(tranDto: tranDto): Promise<Trans> {
    const user = new Trans();
    user.transactionType = tranDto.transactionType;
    user.isProcess = tranDto.isProcess;
    user.isdone = tranDto.isdone;

    return user.save();
    
  }

  async findAll(): Promise<Trans[]> {
    return this.TransModel.findAll({
      include:[{model:User},{model:Feed}]
    });
  }
}

@Injectable()
export class FeedService {
  constructor(
    @InjectModel(Feed) private readonly FeedModel: typeof Feed, 
  ) {}

  async create(feedDto: feedDto): Promise<Feed> {
    const user = new Feed();
    user.isUserHappy = feedDto.isUserHappy;
    user.feedback = feedDto.feedback;

    return user.save();
    
  }

  async findAll(): Promise<Feed[]> {
    return this.FeedModel.findAll({include:[{model:Trans}]});
  }

  findOne(id: string): Promise<Feed> {
    return this.FeedModel.findOne({
      where: {
        id,
      },
    });
  }

  async remove(id: string): Promise<void> {
    const user = await this.findOne(id);
    await user.destroy();
  }
}