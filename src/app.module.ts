import { Module } from '@nestjs/common';
import { from } from 'rxjs';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import{MongooseModule} from '@nestjs/mongoose';
import { CatsModule} from './cats/cats.module';
import { SequelizeModule} from '@nestjs/sequelize';

@Module({
  imports: [SequelizeModule.forRoot({
  dialect: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'root',
  database: 'test',
  autoLoadModels: true,
  synchronize: true,
}), CatsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
